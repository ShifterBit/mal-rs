{
  inputs = {
    utils.url = "github:numtide/flake-utils";
    naersk.url = "github:nmattia/naersk";
    flake-compat = {
      url = "github:edolstra/flake-compat";
      flake = false;
    };
  };

  outputs = { self, nixpkgs, utils, naersk, ... }:
    utils.lib.eachDefaultSystem (system:
      let
        pkgs = nixpkgs.legacyPackages."${system}";
        naersk-lib = naersk.lib."${system}";
      in rec {
        # `nix build`
        packages.mal-rs = naersk-lib.buildPackage {
          pname = "mal-rs";
          root = ./.;
          buildInputs = with pkgs; [ pcre autoconf pkg-config ];
        };

        defaultPackage = packages.mal-rs;

        # `nix run`
        apps.mal-rs = utils.lib.mkApp { drv = packages.mal-rs; };
        defaultApp = apps.mal-rs;

        # `nix develop`
        devShell =
          pkgs.mkShell { nativeBuildInputs = with pkgs; [ rustc cargo pcre autoconf pkg-config]; };
      });
}
