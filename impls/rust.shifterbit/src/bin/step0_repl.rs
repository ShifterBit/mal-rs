use std::io::{stdin, stdout, Write};
use rustyline::error::ReadlineError;
use rustyline::Editor;
// use mal_rs::reader;


fn main() {
  repl();
}

fn repl() {
    let mut rl = Editor::<()>::new();
    if rl.load_history("history.txt").is_err() {
        println!("No previous history.");
    }
    loop {
        let readline = rl.readline("user> ");
        match readline {
            Ok(line) => {
                rl.add_history_entry(line.as_str());
                println!("{}", rep(&line));
            },
            Err(ReadlineError::Interrupted) => {
                println!("CTRL-C");
                break
            },
            Err(ReadlineError::Eof) => {
                println!("CTRL-D");
                break
            },
            Err(err) => {
                println!("Error: {:?}", err);
                break
            }
        }
    }
    rl.save_history("history.txt").unwrap();
}

fn read(text: &str) -> &str {
  text
}

fn eval(text: &str) -> &str {
  text
}

fn print(result: &str) -> &str {
  result

}

fn rep(text: &str) -> &str {
  print(eval(read(text)))
}
