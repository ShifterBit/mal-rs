use regex::Regex;

#[derive(Clone, Copy)]
pub enum Token {
    SquiglyAt,
    Squigly,
    At,
    LeftBracket,
    RightBracket,
    LeftBrace,
    RightBrace,
    Quote,
    Tick,
    Number(f64),
    Bool(bool)

}

pub struct Reader {
    tokens: Vec<Token>,
    position: usize,
}

impl Reader {
    pub fn new() -> Reader {
        Reader {
            tokens: Vec::new(),
            position: 0,
        }
    }

    fn tokenize(&self, source: &str) -> Vec<String> {
        const PATTERN: &str =
            r###"[\s,]*(~@|[\[\]{}()'`~^@]|"(?:\\.|[^\\"])*"?|;.*|[^\s\[\]{}('"`,;)]*)"###;
        let re = Regex::new(PATTERN).unwrap();
        let mut res: Vec<String> = vec![];
        for cap in re.captures_iter(source) {
            if cap[1].starts_with(";") {
                continue;
            }
            res.push(String::from(&cap[1]));
        }
        res
    }

    pub fn read_str(&self, source: &str) -> Vec<String> {
        return self.tokenize(source);
    }

    fn next(&mut self) -> Token {
        self.position += 1;
        self.tokens[self.position]
    }

    fn peek(&self) -> Token {
        self.tokens[self.position]
    }
}
